package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Course;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface CourseService {

    void createCourse(String stringToken,Course course);
    Iterable<Course> getCourse();

    ResponseEntity updateCourse(Long id,String stringToken, Course course);
    ResponseEntity deleteCourse(String stringToken,Long id);
    Iterable<Course> getMyCourse(String stringToken);

}
